const {writeFile, dumpJson, getDateRangeString} = require('./utils');
const xmlbuilder = require('xmlbuilder');

/*

Sample package for reference

<?xml version="1.0" encoding="UTF-8"?>
<Package xmlns="http://soap.sforce.com/2006/04/metadata">
    <types>
        <members>Account</members>
        <name>CustomObject</name>
    </types>
    <version>46.0</version>
</Package>

*/

// TODO: refactor, can be further optimized
const createPackageXml = async (metadataList, dateFilter, users) => {

    let metadataByType = groupByType(metadataList, () => true, () => true);

    await dumpJson(metadataByType, generateName('all', 'json'));
    await writeFile(generateName('all', 'xml'), createXml(metadataByType), 'utf-8');

    const usersFilter = users => metadata => !!users.find(
        user => user.Id === metadata.createdById || user.Id === metadata.lastModifiedById
    );

    metadataByType = groupByType(metadataList, dateFilter, () => true);
    await dumpJson(metadataByType, generateName('all_for_date', 'json'));
    await writeFile(generateName('all_for_date', 'xml'), createXml(metadataByType), 'utf-8');

    let filtered = groupByType(metadataList, dateFilter, usersFilter(users));

    await writeFile(generateName('all-users', 'xml'), createXml(filtered), 'utf-8');

    users.forEach(async user => {
        filtered = groupByType(metadataList, dateFilter, usersFilter([user]));
        await dumpJson(filtered, generateName(user.Username, 'json'));
        await writeFile(generateName(user.Username, 'xml'), createXml(filtered), 'utf-8');
    });
};

const generateName = (name, ext) => {
    return `${getDateRangeString()}_${name}.${ext}`;
};

const groupByType = (metadataList, dateFilter, usersFilter) => {
    // standard fields are not return with a sobject name they belong to
    // also they are not editable, so we can omit them without worrying
    const standardFields = metadata => {
        if (metadata.type === 'CustomField') {
            return metadata.fullName.includes('.');
        }
        return true;
    };

    const grouped = metadataList
        .filter(standardFields)
        .filter(dateFilter)
        .filter(usersFilter)
        .reduce((map, metadata) => {
            if (!(metadata.type in map)) {
                map[metadata.type] = [];
            }
            map[metadata.type].push(metadata.fullName);
            return map;
        }, {});

    // sort
    const sorted = {};
    const sortedKeys = Object.keys(grouped).sort();

    sortedKeys.forEach(key => {
        sorted[key] = grouped[key].sort();
    });

    return sorted;
};

const createXml = (metadataByType) => {
    let root = xmlbuilder.create('Package',
        {xmlns: 'http://soap.sforce.com/2006/04/metadata'}
    );

    Object.keys(metadataByType).forEach(key => {
        let types = root.ele('types');
        metadataByType[key].forEach(type => {
            types.ele('members', {}, type)
        });
        types.ele('name', {}, key)
    });

    return root
        .ele('version', {}, '46.0')
        .end({pretty: true});

};

module.exports = {
    createPackageXml,
}