const moment = require("moment");
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const writeFile = util.promisify(require('fs').writeFile);
const readFile = util.promisify(require('fs').readFile);

const _today = moment();
const parseDateLiteral = literal => {
    switch (literal) {
        case "TODAY": {
            return moment()
                .hours(0)
                .toDate();
        }
        case "YESTERDAY": {
            return moment()
                .days(-1)
                .hours(0)
                .toDate();
        }
        case "WEEK": {
            return moment()
                .days(-7)
                .hours(0)
                .toDate();
        }
        case "MONTH": {
            return moment()
                .month(-1)
                .hours(0)
                .toDate();
        }
        case "ALL": {
            return moment(0).toDate();
        }
        default: {
            return moment(0).toDate();
        }
    }
};

const dumpJson = async (json, file) => {
    return writeFile(file, JSON.stringify(json, null, 2), 'utf-8');
};

const readJson = async path => {
    const file = await readFile(path, 'utf-8');
    return JSON.parse(file);
};

const flatten = arr => {
    return arr.reduce((acc, el) => {
        if (Array.isArray(el)) {
            return [...el, ...acc];
        } else {
            return [el, ...acc]
        }
    }, [])
};

const getDateRangeString = () => {
    return `${process.env.FROM_DATE}_${_today.format('YYYY-MM-DD')}`;
};

const parseUserEmails = csvEmails => {
    if (csvEmails) {
        const emails = csvEmails.split(',')
            .map(email => email.trim());
        return emails;
    }
    return [];
};

module.exports = {
    parseDateLiteral,
    dumpJson,
    readJson,
    flatten,
    readFile,
    writeFile,
    parseUserEmails,
    getDateRangeString,
};
