const util = require('util');
const exec = util.promisify(require('child_process').exec);
const jsforce = require('jsforce');

let _conn;
let _apiCount = 0;

const nativeRequest = jsforce.Connection.prototype.request;
jsforce.Connection.prototype.request = function (request, options, callback) {
    _apiCount++;
    return nativeRequest.call(this, request, options, callback);
};

const authenticate = async () => {
    const {accessToken, instanceUrl, username, id} = await getOrgDetails(process.env.SFDX_ORG);

    _conn = new jsforce.Connection({
        instanceUrl,
        accessToken,
    });

    return _conn;
};

const fetchUsers = async (usernames, usernameRe) => {
    const conditions = getConditions(usernames, usernameRe);

    if(!conditions){
        return [];
    }

    if(!_conn){
        await authenticate();
    }

    const users = await _conn
        .sobject("User")
        .find(
            conditions,
            {
                Id: 1,
                Email: 1,
                Username: 1,
                FirstName: 1,
                LastName: 1
            }
        )
        .execute();

    return users;
};

const getConditions = (usernames, usernameRe) => {
    let conditions = [];
    if (Array.isArray(usernames) && usernames.length > 0) {
        conditions.push({Username: {$in: usernames}})
    }
    if (usernameRe) {
        conditions.push({Username: {$like: usernameRe}})
    }

    if (conditions.length === 1) {
        return conditions[0];
    }

    if (conditions.length === 2) {
        return {
            $or: conditions
        }
    }
    return null;
};


const getOrgDetails = async sfdxOrg => {
    const {stdout, stderr} = await exec(`sfdx force:org:display -u ${sfdxOrg} --json`);

    if (stderr) {
        console.log('error while attempting to authorize', stderr);
        return null;
    }

    return JSON.parse(stdout).result;
};

const apiCallsCount = () => _apiCount;

module.exports = {
    fetchUsers,
    authenticate,
    getOrgDetails,
    apiCallsCount,
};