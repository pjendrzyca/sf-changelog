const { flatten } = require('./utils');

/*  Sample metadata list entry for reference

    {
      "createdById": "0054P00000A64DzQAJ",
      "createdByName": "piotr jendrzyca",
      "createdDate": "1970-01-01T00:00:00.000Z",
      "fileName": "settings/Product.settings",
      "fullName": "Product",
      "id": "",
      "lastModifiedById": "0054P00000A64DzQAJ",
      "lastModifiedByName": "piotr jendrzyca",
      "lastModifiedDate": "1970-01-01T00:00:00.000Z",
      "namespacePrefix": "",
      "type": "Settings"
    }

*/

const FOLDERS_BY_METADATA = {
    'EmailTemplate': 'EmailFolder',
    'Dashboard': 'DashboardFolder',
    'Report': 'ReportFolder',
    'Document': 'DocumentFolder',
};

class MetadataRetriever {
    constructor(connection) {
        this.connection = connection;
    }

    async describeMetadata() {
        const { metadataObjects } = await this.connection.metadata.describe(process.env.API_VERSION);
        return metadataObjects;
    }

    async retrieveAllMetadata() {
        const metadataObjects = await this.describeMetadata();
        const metadataWithChildren = this.buildMetadataWithChildren(metadataObjects);

        const metadataDescribeList = await this.retrieveMetadataDescribe(
            this.buildDescribePromises(metadataObjects)
        );
        const childMetadataDescribeList = await this.retrieveMetadataDescribe(
            this.buildChildDescribePromises(metadataDescribeList, metadataWithChildren)
        );

        const folderDependedMetadataList = await this.retrieveFolderDependentMetadata();

        return flatten([...metadataDescribeList, ...childMetadataDescribeList, ...folderDependedMetadataList]);
    }

    async retrieveFolders() {
        const $promises = Object.values(FOLDERS_BY_METADATA)
            .map(folder => {
                return this.createDescribePromise(folder, null)
            });

        return await this.retrieveMetadataDescribe($promises);
    }

    async retrieveFolderDependentMetadata() {
        const folders = await this.retrieveFolders();

        const mapFolder = (folder, map) => {
            if (!(folder.type in map)) {
                map[folder.type] = []
            }
            map[folder.type].push(folder.fullName)
        };

        const foldersByType = folders.reduce((map, folder) => {
            if (Array.isArray(folder)) {
                folder.forEach(subfolder => mapFolder(subfolder, map));
            }
            else {
                mapFolder(folder, map);
            }

            return map;
        }, {});

        const $promises = Object.keys(FOLDERS_BY_METADATA)
            .reduce((acc, type) => {
                const folder = FOLDERS_BY_METADATA[type];

                if (folder in foldersByType) {
                    const $describeByFolders = foldersByType[folder].map(folderName => {
                        return this.createDescribePromise(type, folderName);
                    });
                    return [...$describeByFolders, ...acc];
                }

                return acc;
            }, []);

        return await this.retrieveMetadataDescribe($promises);
    }

    async retrieveMetadataDescribe($promises) {
        console.log('retrieving metadata - created promises count: ', $promises.length);
        // describe list comes back as 2d array
        // if metadata had no elements it will come back as undefined/null
        try {
            const metadataDescribeList = await Promise.all($promises);
            return metadataDescribeList
                .filter(describeList => !!describeList);
        }
        catch (err) {
            console.log(err)
        }
    }

    createDescribePromise(type, folder) {
        return this.connection.metadata.list([{ type, folder }], process.env.API_VERSION);
    }

    buildMetadataWithChildren(metadataObjects) {
        return metadataObjects
            .filter(metadata => {
                return Array.isArray(metadata.childXmlNames)
            })
            .reduce((acc, metadata) => {
                acc[metadata.xmlName] = metadata.childXmlNames;
                return acc;
            }, {});
    }

    buildDescribePromises(metadataObjects) {
        return metadataObjects.reduce(($arr, metadata) => {
            // ignoring folder depended metadata
            // this will be handled in separate call
            if (metadata.inFolder) {
                return $arr;
            }

            return [this.createDescribePromise(metadata.xmlName, null), ...$arr]
        }, [])
    }

    buildChildDescribePromises(metadataDescribeList, metadataWithChildren) {
        const createPromiseForDescribe = (describe) => {
            if (describe.type in metadataWithChildren) {
                return metadataWithChildren[describe.type]
                    .map(child => {
                        return this.createDescribePromise(child, describe.fullName);
                    })

            }
            return [];
        };

        return metadataDescribeList.reduce((acc, describeList) => {

            let $all = [];
            if (Array.isArray(describeList)) {
                describeList.forEach(describe => {
                    $all = [...createPromiseForDescribe(describe), ...$all];
                })
            }
            else {
                $all = [...createPromiseForDescribe(describeList), ...$all];
            }
            return [...$all, ...acc];

        }, [])
    }
}

module.exports = MetadataRetriever;