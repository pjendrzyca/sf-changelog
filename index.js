const MetadataRetriever = require("./lib/retrieve");
const {parseUserEmails, getDateRangeString} = require("./lib/utils");
const {authenticate, fetchUsers, apiCallsCount} = require("./lib/api");
const {createPackageXml} = require("./lib/packageBuilder");
const program = require("commander");
const fs = require("fs");

// TODO:
// 1. consider graphical interface for easier package creation
// 2. introduce cache logic, maybe some nosql db to cache the calls as they are expensive
// ~1000 callouts for almost empty org, might hit limits with bigger ones
// 3. optimize callouts count - find out if its possible to query for more then 1 metadata at once
// 4. when fetching standard field the full name does not contain the sobject name
// introduce filter to handle this
// 5.

program
    .version("0.1 - SF Changelog")
    .option("-o, --org-name <orgName>", "sfdx org alias")
    .option(
        "-d, --from-date <fromDate>",
        "start date for which the package should be generated in yyyy-mm-dd format"
    )
    .option(
        "-u, --users [users]",
        "comma separated list of sf usernames for which the package should be generated"
    )
    .option(
        "-r, --user-regex [userRegex]",
        "regex to filter user emails by. Allows '%' wildcard. Example '%@forefrontcorp%'"
    )
    .option(
        "-t, --time [time]",
        "specific start time to used with the date in HH:mm format. Where hours are in 24h format"
    )
    .option(
        "--output [output]",
        "output folder name"
    )
    .option(
        "--apiVersion [apiVersion]",
        "salesforce api version. Default 45.0"
    )
    .parse(process.argv);

const main = async () => {
    // const allMetadata = await readJson('all.json');

    if (!program.orgName || !program.fromDate) {
        console.error(
            "[ERROR]",
            "You need to specify both org and data. See sf-changelog --help for more information"
        );
        return;
    }

    process.env.SFDX_ORG = program.orgName;
    process.env.FROM_DATE = program.fromDate;
    process.env.OUTPUT_DIR = program.output ? './' + program.output : './' + getDateRangeString() + '_package';
    process.env.API_VERSION = program.apiVersion || '45.0';

    // creating output dir and changing cwd
    if (!fs.existsSync(process.env.OUTPUT_DIR)) {
        fs.mkdirSync(process.env.OUTPUT_DIR);
    }

    process.chdir(process.env.OUTPUT_DIR);

    const usernames = parseUserEmails(program.users);
    console.log('passed usernames', usernames);
    const regex = program.userRegex;

    console.log('authenticating');
    const conn = await authenticate();
    console.log('[INFO]', 'callout count', apiCallsCount());
    console.log('retrieving metadata');

    const service = new MetadataRetriever(conn);
    const allMetadata = await service.retrieveAllMetadata();
    console.log('[INFO]', 'callout count', apiCallsCount());

    const filter = metadata => {
        let isCreatedWithin = new Date(metadata.createdDate) > new Date(process.env.FROM_DATE);
        let isModifiedWithin = metadata.lastModifiedDate ?
            new Date(metadata.lastModifiedDate) > new Date(process.env.FROM_DATE) : false;

        return isCreatedWithin || isModifiedWithin;
    };

    console.log('fetching users');
    const users = await fetchUsers(usernames, regex);
    console.log('fetched users', users);
    console.log('[INFO]', 'callout count', apiCallsCount());

    console.log('creating package');
    await createPackageXml(allMetadata, filter, users);
    console.log('[INFO]', 'callout count', apiCallsCount());

    console.log('Finished');
};

main();
